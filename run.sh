#!/bin/sh

if ! type mvn &>/dev/null; then
	error "mvn is not installed"
else
	info "mvn is installed"
	debug "$(mvn -version)"
fi

mvn -U -B -Dmaven.repo.local=${WERCKER_CACHE_DIR} test
